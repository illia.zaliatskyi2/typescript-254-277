import { combineReducers } from 'redux'
import { todosReducer } from './todoReducer'
import { StoreState } from '../../types/IStoreState'

export const index = combineReducers<StoreState>({
  todos: todosReducer
})
