import { Dispatch } from 'redux'
import axios from 'axios'
import { Todo } from '../../types/ITodo'
import { FetchTodosAction } from '../../types/IFetchTodosAction'
import { ActionTypes } from '../../types/actionTypes'

const todosUrl = 'https://jsonplaceholder.typicode.com/todos'

export const fetchTodos = () => {
    return async (dispatch: Dispatch) => {
        const response = await axios.get<Todo[]>(todosUrl)

        dispatch<FetchTodosAction>({
            type: ActionTypes.fetchTodos,
            payload: response.data.slice(0, 30)
        })
    }
}