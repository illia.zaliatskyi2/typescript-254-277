import { DeleteTodoAction } from '../../types/IdeleteTodoAction'
import { ActionTypes } from '../../types/actionTypes'

export const deleteTodo = (id: number): DeleteTodoAction => {
    return {
        type: ActionTypes.deleteTodo,
        payload: id
    }
}