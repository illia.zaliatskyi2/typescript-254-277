import { fetchTodos } from './fetchTodos'
import { deleteTodo } from './deleteTodo'

export {
    fetchTodos,
    deleteTodo
}