import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import { index } from './reducers'

const store = createStore(index, applyMiddleware(thunk))

export default store