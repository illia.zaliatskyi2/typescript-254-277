import { FetchTodosAction } from './IFetchTodosAction'
import { DeleteTodoAction } from './IdeleteTodoAction'

export enum ActionTypes {
    fetchTodos,
    deleteTodo
}

export type Action = FetchTodosAction | DeleteTodoAction