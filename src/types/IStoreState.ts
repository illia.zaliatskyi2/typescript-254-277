import { Todo } from './ITodo'

export interface StoreState {
    todos: Todo[]
}
