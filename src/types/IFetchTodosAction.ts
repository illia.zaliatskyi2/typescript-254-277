import { ActionTypes } from './actionTypes'
import { Todo } from './ITodo'

export interface FetchTodosAction {
    type: ActionTypes.fetchTodos,
    payload: Todo[]
}
