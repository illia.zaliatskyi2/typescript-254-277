import { Todo } from './ITodo'
import { deleteTodo, fetchTodos } from '../redux/actions'

export interface AppProps {
    todos: Todo[]
    fetchTodos: Function
    deleteTodo: typeof deleteTodo
}
