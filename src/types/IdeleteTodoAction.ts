import { ActionTypes } from './actionTypes'

export interface DeleteTodoAction {
    type: ActionTypes.deleteTodo
    payload: number
}
