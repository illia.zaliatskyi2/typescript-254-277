import React from 'react'
import './App.scss'
import { connect } from 'react-redux'
import { StoreState } from './types/IStoreState'
import { fetchTodos, deleteTodo } from './redux/actions'
import { Todo } from './types/ITodo'
import { AppProps } from './types/IAppProps'

const Content = <T extends AppProps>(props: T): JSX.Element => {

    const renderList = (): JSX.Element[] => {
        return props.todos.map(({ id, title, completed }) => {
            return <li key={id} onClick={() => {removeToDo(id)}}>{title}</li>
        })
    }

    const removeToDo = (id: number): void => {
        props.deleteTodo(id)
    }

    const fetchTodosClick = (): void => {
        props.fetchTodos()
    }

    return (
        <div className="App">
            here is our app and todos:
            <br/>
            { props.todos.length === 0 && <button onClick={fetchTodosClick}>get todos</button> }
            { props.todos.length > 0 && <ul>{renderList()}</ul> }
        </div>
    )
}
const mapStateToProps = ({todos}: StoreState): { todos: Todo[] } => {
    return { todos }
}

export const App = connect(mapStateToProps, { fetchTodos, deleteTodo })(Content)
